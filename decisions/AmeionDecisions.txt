country_decisions = {
	G52_adopt_taychendi = {
		potential = {
			mission_completed = G52_against_warlords
			NOT = { has_country_flag = ameion_adopt }
		}
		provinces_to_highlight = {
		    region = taychend_region
			owned_by = ROOT
			NOT = { religion = kheionism }
			NOT = { is_island = yes }
		}
		allow = {
			mission_completed = G52_against_warlords
			has_institution = renaissance
			calc_true_if = {
			 	all_owned_province = { 
					region = taychend_region
					religion = kheionism
					NOT = { is_island = yes }
				}
				amount = 5
			}
			2520 = {
				owned_by = ROOT
				is_core = ROOT
				religion = kheionism
			}
		}
		effect = {
			set_country_flag = ameion_adopt
			2520 = { change_culture = klereyen }
			add_country_modifier = {
				name = G52_straw_brick
				duration = 9125
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	G52_precursor_excavation = {
		potential = {
			tag = G52
			religion = kheionism
			NOT = { has_country_modifier = G52_mage_8 }
			NOT = { has_country_modifier = G52_ai_dig }
		}
		provinces_to_highlight = {
		    province_group = excavation_sites
			NOT = { has_province_modifier = G52_excavation }
			NOT = { has_province_modifier = G52_excavated }
		}
		allow = {
			tag = G52
			custom_trigger_tooltip = {
				tooltip = ameion_no_excavation
				NOT = { kheionai_superregion = { has_province_modifier = G52_excavation } }
				NOT = { greater_taychend_superregion = { has_province_modifier = G52_excavation } }
			}
			manpower = 5
			adm_power = 50
			mil_power = 50
			estate_loyalty = { estate = estate_mages loyalty = 30 }
			if = {
				limit = { has_country_flag = ameion_mages }
				OR = {
					any_owned_province = { 
						province_group = excavation_sites
						NOT = { has_province_modifier = G52_excavation }
						NOT = { has_province_modifier = G52_excavated }
					}
					any_subject_country = {
						any_owned_province = {
							province_group = excavation_sites
							NOT = { has_province_modifier = G52_excavation }
							NOT = { has_province_modifier = G52_excavated }
						}
					}
					custom_trigger_tooltip = {
						tooltip = ameion_tacenie
						has_country_flag = ameion_mages
					}
				}
			}
			else = {
				OR = {
					any_owned_province = {
						province_group = excavation_sites
						NOT = { has_province_modifier = G52_excavation }
						NOT = { has_province_modifier = G52_excavated }
					}
					any_subject_country = {
						any_owned_province = {
							province_group = excavation_sites
							NOT = { has_province_modifier = G52_excavation }
							NOT = { has_province_modifier = G52_excavated }
						}
					}
				}
			}
		}
		effect = {
			country_event = { id = flavor_ameion.100 }
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	G52_larankar_treaty = {
		potential = {
			tag = G52
			mission_completed = G52_prepare_push
			NOT = { mission_completed = G52_civilization_taychend }
			NOT = { has_country_flag = ameion_treaty }
			exists = G64
		}
		provinces_to_highlight = {
		    region = taychend_region
			OR = {
				country_or_non_sovereign_subject_holds = G64
				owner = { alliance_with = G64 }
			}
			NOT = {
				area = marhed_varanya_area
				area = bettagophira_area
				culture = kalavendhi
			}
		}
		allow = {
			tag = G52
			mission_completed = G52_prepare_push
			NOT = { mission_completed = G52_civilization_taychend }
			taychend_region = {
				OR = {
					country_or_non_sovereign_subject_holds = G64
					owner = { alliance_with = G64 }
				}
				NOT = {
					area = marhed_varanya_area
					area = bettagophira_area
					culture = kalavendhi
				}
			}
			NOT = { is_at_war = yes }
			dip_power = 200
		}
		effect = {
			set_country_flag = ameion_treaty
			add_dip_power = -200
			G64 = { country_event = { id = flavor_ameion.29 days = 1 } }
			custom_tooltip = ameion_treaty_tt
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
}