country_decisions = {

	roadwarrior_upgrades = {
		major = yes
		potential = {
			tag = H91
			has_reform = roadwarrior_great_convoy
		}
		allow = {
			if = {
				limit = { NOT = { has_reform = roadwarrior_upgrades_on_the_go } }
				capital_scope = {
					OR = {
						has_terrain = dwarven_hold
						has_terrain = dwarven_hold_surface
					}
				}
			}
			NOT = { has_country_modifier = roadwarrior_upgrading }
		}
		effect = {
			country_event = { id = roadwarrior.1 }
		}
	}

	roadwarrior_teleport = {
		major = yes
		potential = {
			tag = H91
			has_reform = roadwarrior_great_convoy
		}
		allow = {
			capital_scope = {
				OR = {
					has_terrain = dwarven_hold
					has_terrain = dwarven_hold_surface
					has_terrain = dwarven_road
				}
			}
			NOT = { is_at_war = yes }
			NOT = { has_country_modifier = roadwarrior_recent_overdrive }
		}
		effect = {
			country_event = { id = roadwarrior.9 }
		}
	}

	roadwarrior_settle_down = {
		major = yes
		potential = {
			tag = H91
			has_reform = roadwarrior_great_convoy
		}
		allow = {
			capital_scope = {
				OR = {
					has_terrain = dwarven_hold
					has_terrain = dwarven_hold_surface
				}
			}
			NOT = { has_country_modifier = roadwarrior_upgrading } 
		}
		effect = {
			country_event = { id = roadwarrior.10 }
		}
	}

	roadwarrior_get_tribal_land = {
		major = yes
		potential = {
			tag = H91
			capital_scope = {
				NOT = { tribal_land_of = ROOT }
			}
		}
		allow = {
			has_reform = roadwarrior_great_convoy
			if = {
				limit = {
					capital_scope = {
						OR = {
							has_terrain = dwarven_hold
							has_terrain = dwarven_hold_surface
						}
					}
					adm_power = 100
					manpower = 10
					treasury = 200
				}
			}
			else_if = {
				limit = {
					capital_scope = {
						has_terrain = dwarven_road
					}
					adm_power = 10
					manpower = 1
					treasury = 10
				}
			}
			else = {
				adm_power = 20
				manpower = 2
				treasury = 20
			}
		}
		effect = {
			if = {
				limit = {
					capital_scope = {
						OR = {
							has_terrain = dwarven_hold
							has_terrain = dwarven_hold_surface
						}
					}
				}
				add_adm_power = 100
				add_manpower = 10
				add_treasury = 200
			}
			else_if = {
				limit = {
					capital_scope = {
						has_terrain = dwarven_road
					}
				}
				add_adm_power = 10
				add_manpower = 1
				add_treasury = 10
			}
			else = {
				add_adm_power = 20
				add_manpower = 2
				add_treasury = 20
			}
			capital_scope = {
				change_tribal_land = ROOT
			}
		}
	}
}